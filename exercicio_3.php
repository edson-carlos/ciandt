<?php

/* 
 * @author Edson Carlos de Oliveira
 */

class Extension
{
    public $extensions_list = [];
    
    public function loadArray($filesName)
    {
        if (empty($filesName) || gettype($filesName) !== 'array') {
            exit('Error. Array type and not empty is necessary to load with successfully!');
        }
        
        foreach ($filesName as $fileName) 
        {
            $this->extractExtensions($fileName);
        }
        
    }
    
    private function extractExtensions($fileName)
    {
        array_push($this->extensions_list, strrchr($fileName, '.'));
    }
    
    public function alphabeticOrder()
    {
        sort($this->extensions_list);
        
    }
    
    public function print_list()
    {
        array_map(function($ext) {
            echo "<li>{$ext}</li>";
        }, $this->extensions_list);
    }
}

$filesName = [
    'music.mp4',
    'video.mov',
    'imagem.jpeg',
    'file.class.php',
];

$extension = new Extension;
$extension->loadArray($filesName);
$extension->alphabeticOrder();
?>
<ol>
    <?php $extension->print_list(); ?>
</ol>