<?php

/* 
 * @author Edson Carlos de Oliveira
 */

// Simple função para análise do retorno da entidade gramatical  
function entity_analise($entity) {
    switch ($entity) {
        case 'fem':
            return 'da';
        case 'masc':
            return 'do';
        case 'masc_p':
            return 'dos';
        case 'fem_p':
            return 'das';
        default:
            return 'de';
    }
}

$location = array(
    [
        'country' => 'Espanha',
        'capital' => 'Madrid',
        'entity' => 'fem'
    ],
    [
        'country' => 'Brasil',
        'capital' => 'Brasilia',
        'entity' => 'masc'
    ],
    [
        'country' => 'Polônia',
        'capital' => 'Varsôvia',
        'entity' => 'fem'
    ],
    [
        'country' => 'EUA',
        'capital' => 'Washington',
        'entity' => 'masc_p'
    ],
    [
        'country' => 'França',
        'capital' => 'Paris',
        'entity' => 'fem'
    ],
    [
        'country' => 'Ilhas Cayman',
        'capital' => 'Georgetown',
        'entity' => 'fem_p'
    ]
);

// Uso de uma função anônima para ordenar o array associativo
usort($location, function($country_a, $country_b) {
    return $country_a['country'] > $country_b['country'];
});

array_map(function($country_data) {
    $preposition = entity_analise ($country_data['entity']);
    echo "A capital {$preposition} <b>{$country_data['country']}</b> é <b>{$country_data['capital']}</b><br/>";
}, $location);

