<?php

define (BASE_FILE, __DIR__ . 'registros.txt');

$data = [];
$erro = [];
$sucesso = [];
$erro_style = "style='border-color: red'";

function clean_input($input) 
{
  return trim( stripslashes( htmlspecialchars( $input )));
}

function apagar_conteudo()
{
    $array = file(BASE_FILE);
    foreach ($array as $key => $value) {
        unset($array[$key]);
    }
    file_put_contents(BASE_FILE, implode(PHP_EOL, $array));

}

function pesquisar($key, $value)
{
    if (file_exists( BASE_FILE)) {
        $arquivo  = fopen( BASE_FILE, 'r');
        while (!feof($arquivo)) {
          $linha = fgets($arquivo);
          $json = json_decode($linha);
          if ($json && $json->$key == $value) {
              return true;
          }
       }
       fclose($arquivo);
    } else {
       return false;
    }
}
function registrar($data, &$erro, &$sucesso)
{
//    apagar_conteudo();exit;
    $email_ja_existe = pesquisar('email', $data['email']);
    $login_ja_existe = pesquisar('login', $data['login']);
    
    if ($email_ja_existe) {
        $erro['email'] = "O endereço de e-mail já está cadastrado!";
    }  
    if($login_ja_existe) {
        $erro['login'] = "O login já está cadastrado!";
    }
    if ($erro) {
        return false;
    }
    
    $novo_num = count(file(BASE_FILE)) + 1;
    $data = ['id' => $novo_num]  + $data;
    
    
    $data_json = json_encode($data) . PHP_EOL;
//    print_r($data_json);exit;
    $file_name = BASE_FILE;
    $file = fopen($file_name, 'a');
    fwrite($file, $data_json);
    fclose($file);
    $sucesso['msg'] = "Registro efetuado com sucesso! Número de registro: {$novo_num}";
    return true;
}

function validar_telefone($fone)
{
    $fone = preg_replace('/[^0-9]/','', $fone);
    if (substr($fone, 0, 2) == 55) {
        $fone = str_replace('55', '', $fone);
    }
    $num = strlen($fone);
    
    if ($num < 10) {
        return false;
    }
    return true;
}

if ($_POST) {    
    $nome = $sobrenome = $email = $telefone = $login = $pass = '';
    
    if ($_POST['nome']) {
        $data['nome'] = clean_input($_POST['nome']);
    } else {
        $erro['nome'] = "O nome é um campo de preenchimento obrigatório!";
    }
    if ($_POST['sobrenome']) {
        $data['sobrenome'] = clean_input($_POST['sobrenome']);
    } else {
        $erro['sobrenome'] = "O sobrenome é um campo de preenchimento obrigatório!";
    }
    if ($_POST['email']) {
        if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $data['email'] = clean_input($_POST['email']); 
        } else {
            $erro['email'] = "Informe um email válido!";
        }
    } else {
        $erro['email'] = "O e-mail é um campo de preenchimento obrigatório!";
    }
    if ($_POST['telefone']) {
        $data['telefone'] = clean_input($_POST['telefone']);
        if ( ! validar_telefone($data['telefone'])) {
            $erro['telefone'] = "Informe um número de telefone válido!";
            $data['telefone'] = clean_input($_POST['telefone']);
        }
    } else {
        $erro['telefone'] = "O telefone é um campo de preenchimento obrigatório!";
    }
    if ($_POST['login']) {
        $data['login'] = clean_input($_POST['login']);
    } else {
        $erro['login'] = "O login é um campo de preenchimento obrigatório!";
    }
    if ($_POST['pass']) {
        $data['pass'] = clean_input($_POST['pass']);
    } else {
        $erro['pass'] = "A senha é um campo de preenchimento obrigatório!";
    }
    
    if (empty($erro)) {
        $data['pass'] = sha1($data['pass']);
        $registro = registrar($data, $erro, $sucesso);
        if ($registro) {
            $data = [];
        }
    }
}
?>  
<html>
    <head>
        <style>
            label {
                display: inline-block;
                width: 150px;
                text-align: right;
                margin-right: 15px;
                margin-bottom: 10px;
            }
            input {
                width: 400px;
                height: 30px;
            }
            span.erro {
                background-color: goldenrod;
                color: white;
                padding: 5px;
            }
            span.sucesso {
                display: block;
                background-color: limegreen;
                color: white;
                padding: 10px;
                font-size: 15px;
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <?php if (isset($sucesso['msg'])): ?>
        <span class="sucesso"><?= $sucesso['msg'] ?></span>
        <?php endif; ?>
        <form method="POST">
            <fieldset><legend>Cadastro</legend>
                <label for="nome">Nome: </label>
                <input type="text" <?= isset($erro['nome']) ? $erro_style : '' ?> name="nome" placeholder="<?= isset($erro['nome']) ?$erro['nome'] :'Informe seu nome...' ?>" value="<?= isset($data['nome']) ? $data['nome'] : '' ?>"  />
                <?= isset($erro['nome']) ? "<span class='erro'>{$erro['nome']}</span>" : '' ?>
                <br/>
                <label for="sobrenome">Sobrenome: </label>
                <input type="text" <?= isset($erro['sobrenome']) ? $erro_style : ''?> name="sobrenome" placeholder="<?= isset($erro['sobrenome']) ?$erro['sobrenome'] :'Informe seu sobrenome...' ?>" value="<?= isset($data['sobrenome']) ? $data['sobrenome'] : '' ?>"/>
                <?= isset($erro['sobrenome']) ? "<span class='erro'>{$erro['sobrenome']}</span>" : '' ?>
                <br/>
                <label for="email">E-mail: </label>
                <input type="email" <?= isset($erro['email']) ? $erro_style : ''?> name="email" placeholder="<?= isset($erro['email']) ?$erro['email'] :'Informe seu endereço de e-mail...' ?>" value="<?= isset($data['email']) ? $data['email'] : '' ?>" />
                <?= isset($erro['email']) ? "<span class='erro'>{$erro['email']}</span>" : '' ?>
                <br/>
                <label for="tel">Telefone com DDD: </label>
                <input type="tel" <?= isset($erro['telefone']) ? $erro_style : ''?> name="telefone" placeholder="<?= isset($erro['telefone']) ?$erro['telefone'] :'Informe seu número de telefone...' ?>" value="<?= isset($data['telefone']) ? $data['telefone'] : '' ?>" />
                <?= isset($erro['telefone']) ? "<span class='erro'>{$erro['telefone']}</span>" : '' ?>
                <br/>
                <label for="login">Login: </label>
                <input type="text" <?= isset($erro['login']) ? $erro_style : ''?> name="login" placeholder="<?= isset($erro['login']) ?$erro['login'] :'Informe um login...' ?>" value="<?= isset($data['login']) ? $data['login'] : '' ?>" />
                <?= isset($erro['login']) ? "<span class='erro'>{$erro['login']}</span>" : '' ?>
                <br/>
                <label for="pass">Senha</label>
                <input type="password" <?= isset($erro['pass']) ? $erro_style : '' ?> name="pass" placeholder="<?= isset($erro['pass']) ?$erro['pass'] :'Digite uma senha...' ?>" value="<?= isset($data['pass']) ? $data['pass'] : '' ?>" />
                <?= isset($erro['pass']) ? "<span class='erro'>{$erro['pass']}</span>" : '' ?>
                <br/>
                <label></label>
                <input style="margin-top: 10px;" type="submit" value="Registrar"/>
            </fieldset>
        </form>
    </body>
</html>